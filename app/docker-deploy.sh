#! bin/bash

# login to cloud server
# for droplet on digital ocean
ssh root@104.248.125.43

# docker login for dockerhub repo
docker login

# this will pull and run the docker image from dockerhub
# version-buildnumber is $IMAGE_NAME from pipeline
docker run -p3000:3000 danielleh/my-repo:nodeapp-<1.1.0-1>
