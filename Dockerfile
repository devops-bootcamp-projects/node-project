# image for the env
FROM node:16-alpine

# create working directory in the container
RUN mkdir -p /usr/app
# copy everything in local app directory to container app directory
COPY app/ /usr/app/

# set working directory for the container
WORKDIR /usr/app/
# expose port for the container
EXPOSE 3000

# npm install for the dependencies
RUN npm install
# command to start up the app
CMD ["node", "server.js"]